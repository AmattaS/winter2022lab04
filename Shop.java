import java.util.Scanner;
public class Shop
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		Pizza [] pizzas = new Pizza[4];
		for (int i = 0; i<3; i++)
		{
			// Calling constructor
			
			System.out.println("What size?");
			String s = sc.next();
			
			System.out.println("What topping?");
			String t = sc.next();
			
			System.out.println("What type of meat?");
			String m = sc.next();
			pizzas[i] = new Pizza(s, t, m);
		}

		
		//Printing the fields of the last product
		System.out.println();
		System.out.println(pizzas[2].getSize());
		System.out.println(pizzas[2].topping());
		System.out.println(pizzas[2].meat());
		
		//instance method
		pizzas[2].isHealthy();
		
		//Changing last product
		System.out.println("modify last pizza");
		
		System.out.println("What size?");
			pizzas[2].setSize(sc.next());
			
			System.out.println("What topping?");
			pizzas[2].setTop(sc.next());
		//Printing 3 fields 
		System.out.println();
		System.out.println(pizzas[2].getSize());
		System.out.println(pizzas[2].topping());
		System.out.println(pizzas[2].meat());
			
	}
}